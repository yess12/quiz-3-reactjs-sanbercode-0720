import React from 'react';
import './App.css';
import {
  BrowserRouter as Router
} from "react-router-dom";
import Routes from './quiz/Routes'
import MovieListEditor from './quiz/sudah-login/SudahLogin';
import DaftarBuah from './Contoh2';

function App() {
  return (
    <Router>
      <Routes />
    </Router>
  )
}

export default App;
