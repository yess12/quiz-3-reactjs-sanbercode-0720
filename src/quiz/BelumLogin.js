import React, {useState, useEffect} from "react"
import axios from "axios"

const MovieList = () => {

    const [movieList, setMovieList] = useState(null)

    useEffect( () => {
        if (movieList === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                setMovieList(res.data.map(el=>{ return {id: el.id, title: el.title, description: el.description, duration: el.duration, genre: el.genre, rating: el.rating}} ))
            })
        }
    }, [movieList])

    return(
        <>
            <h1 style={{textAlign: "center"}}>Daftar Film Film Terbaik</h1>
            {
                movieList != null && movieList.map((item, index) => {
                    return(
                        <div key={{index}}>
                            <h2>{item.title}</h2> <br />
                            <b>Rating {item.rating}</b> <br />
                            <b>Durasi: {item.duration}</b> <br />
                            <b>genre: {item.genre}</b> <br /> <br />
                            <b>deskripsi:</b> {item.description} <br /> <br />
                        </div>
                    )
                })
            }
        </>
    )
}

export default MovieList