import React, {Component} from 'react';

class Nama extends Component {
    render() {
    return <p><strong style={{width: "100px"}}>1. Nama: {this.props.nama}</strong></p>
    }
}

class Email extends Component {
    render() {
        return <p><strong style={{width: "100px"}}>2. Email: {this.props.email}</strong></p>
    }
}

class Sistem extends Component {
    render() {
        return <p><strong style={{width: "100px"}}>3. Sistem Operasi yang digunakan: {this.props.sistem}</strong></p>
    }
}

class Gitlab extends Component {
    render() {
        return <p><strong style={{width: "100px"}}>4. Akun Gitlab: {this.props.gitlab}</strong></p>
    }
}

class Telegram extends Component {
    render() {
        return <p><strong style={{width: "100px"}}>5. Akun Telegram: {this.props.telegram}</strong></p>
    }
}

var person = [{nama: "Yessica", email: "yesletisia@gmail.com", sistem: "Microsoft Windows", gitlab: "yess12", telegram: "yess1205"}]

class About extends Component {
    render() {
        return (
          <>
            {person.map(el=> {
              return (
                <div style={{padding: "10px", border: "1px solid #ccc"}}>
                    <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                    <Nama nama={el.nama}/> 
                    <Email email={el.email}/> 
                    <Sistem sistem={el.sistem}/> 
                    <Gitlab gitlab={el.gitlab}/> 
                    <Telegram telegram={el.telegram}/> 
                </div>
              )
            })}
          </>
        )
    }
}

export default About