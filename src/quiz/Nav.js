import React, { useContext } from "react"
import { Link } from "react-router-dom";
// import "./Nav.css"

const Nav = () =>{

  return(
    <nav style={{display: "flex"}}>
      <ul style={{listStyle: "none"}}>
        <li style={{display: "inline"}}>
          <Link to="/">Home</Link>
        </li>
        <li style={{display: "inline"}}>
          <Link to="/about">About</Link>
        </li>
        <li style={{display: "inline"}}>
          <Link to="/login">Login</Link>
        </li >
      </ul>
    </nav>
  )
}

export default Nav
