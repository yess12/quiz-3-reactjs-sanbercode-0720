import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import MovieList from './BelumLogin';
import About from './about/About'
import MovieListEditor from './sudah-login/SudahLogin'
import Nav from './Nav'
import logo from '../public/img/logo.png'
import pattern from "../public/img/pattern.jpg"
import Login from "./login/Login"
import {LoginForm} from "./login/LoginForm"

export default function App() {
    return (
    <>
        <div style={{
            backgroundColor: "white",
            height: "60px",
            display: "flex",
            justifyContent: "space-between", 
            padding: "5px",
            position: "fixed",
            top: "0px",
            right: "0px",
            left: "0px",
            zIndex: 1
            }}>
            <img src={logo} alt="logo" style={{width: "200px"}} />
        </div>
        <div style={{backgroundImage: `url(${pattern})`}}>
            <div style={{backgroundColor: "white", marginLeft: "200px", marginRight: "200px", position: "relative", top: "70px", padding: "20px", marginBottom: "100px"}}>
                <Nav/>
                <Switch>
                    <Route exact path="/">
                        <MovieList />
                    </Route>

                    <Route path="/about">
                        <About />
                    </Route>

                    <Route path="/login">
                        <Login />
                    </Route>

                    <Route path="/SudahLogin">
                        <MovieListEditor />
                    </Route>
                    
                </Switch>
            </div>
        </div>
    </>
    )
}