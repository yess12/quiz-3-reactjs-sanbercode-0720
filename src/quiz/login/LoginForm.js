import React, {useContext, useState} from "react"
import {LoginContext} from './LoginContext'
import { useHistory } from 'react-router-dom'
import MovieListEditor from "../sudah-login/SudahLogin"

 export const LoginForm = () => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [login, setLogin] = useContext(LoginContext)

    const handleSubmit = (event) =>{
      event.preventDefault()
      setLogin([...login, {username, password}])
      setUsername("")
      setPassword("")
    }

    const history = useHistory();

    const routeChange = () => {
      let path = `./SudahLogin`;
      history.push(path);
    }

    const handleUsernameChange = (event) =>{
        setUsername(event.target.value)
    }
    
    const handlePasswordChange = (event) =>{
        setPassword(event.target.value)
    }

    return(
        <>
          <form onSubmit={handleSubmit}>
            <input type="text" value={username} onChange={handleUsernameChange} />
            <input type="text" value={password} onChange={handlePasswordChange} />
            <button onClick={routeChange}>submit</button>
          </form>
        </>
    )
    
}
