import React, {useState, useEffect} from "react"
import axios from "axios"

const MovieListEditor = () => {
    const [movieListEditor, setMovieListEditor] = useState(null)
    const [input, setInput] = useState({title: "", description: "", year: "", duration: 0, genre: "", rating: 1})
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm]  =  useState("create")


    useEffect( () => {
        if (movieListEditor === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                setMovieListEditor(res.data.map(el=>{ return {id: el.id, title: el.title, description: el.description, year: el.year, duration: el.duration, genre: el.genre, rating: el.rating}} ))
            })
        }
    }, [movieListEditor])

    const sorted = [movieListEditor].sort((a, b) => {
        return b.rating - a.rating;
    });

    const handleDelete = (event) => {
        let idMovieListEditor = parseInt(event.target.value)
    
        let newMovieListEditor = movieListEditor.filter(el => el.id !== idMovieListEditor)
    
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovieListEditor}`)
        .then(res => {
          console.log(res)
        })
              
        setMovieListEditor([...newMovieListEditor])
        
    }
      
    const handleEdit = (event) =>{
        let idMovieListEditor = parseInt(event.target.value)
        let movieList = movieListEditor.find(x=> x.id === idMovieListEditor)
        setInput({title: movieList.title, description: movieList.description, year: movieList.year, duration: movieList.duration, genre: movieList.genre, rating: movieList.rating})
        setSelectedId(idMovieListEditor)
        setStatusForm("edit")
      }
    
    const handleChange = (event) =>{
        let typeOfInput = event.target.name
    
        switch (typeOfInput){
            case "title":
            {
                setInput({...input, title: event.target.value});
                break
            }
            case "description":
            {
                setInput({...input, description: event.target.value});
                break
            }
            case "year":
            {
                setInput({...input, year: event.target.value});
                break
            }
            case "duration":
            {
                setInput({...input, duration: event.target.value});
                break
            }
            case "genre":
            {
                setInput({...input, genre: event.target.value});
                break
            }
            case "rating":
            {
                setInput({...input, rating: event.target.value});
                break
            }
        default:
            {break;}
        }
    }

    const handleSubmit = (event) =>{
        event.preventDefault()
    
        let title = input.title
        let description = input.description
        let year = input.year.toString()
        let genre = input.genre     
    
        if (title.replace(/\s/g,'') !== "" && description.replace(/\s/g,'') !== "" && year.replace(/\s/g,'') !== "" && genre.replace(/\s/g,'') !== ""){      
          if (statusForm === "create"){        
            axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title: input.title, description: input.description, year: input.year, duration: input.duration, genre: input.genre, rating: input.rating})
            .then(res => {
                setMovieListEditor([
                  ...movieListEditor, 
                  { id: res.data.id, 
                    title: input.title, 
                    description: input.description,
                    year: input.year,
                    duration: input.duration,
                    genre: input.genre,
                    rating: input.rating,
                  }])
            })
          }else if(statusForm === "edit"){
            axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {title: input.title, description: input.description, year: input.year, duration: input.duration, genre: input.genre, rating: input.rating})
            .then(() => {
                let movieList = movieListEditor.find(el=> el.id === selectedId)
                movieList.title = input.title
                movieList.description = input.description
                movieList.year = input.year
                movieList.duration = input.duration
                movieList.genre = input.genre
                movieList.rating = input.rating
                setMovieListEditor([...movieListEditor])
            })
          }
          
          setStatusForm("create")
          setSelectedId(0)
          setInput({title: "", description: "", year: "", duration: 0, genre: "", rating: 0})
        }

      }

    return (
        <>
            <h1 style={{textAlign: "center"}}>Daftar Film Film Terbaik</h1>
            {
                movieListEditor != null && movieListEditor.map((item, index) => {
                    return(
                        <div key={index}>
                            <h2>{item.title}</h2> <br />
                            <b>Rating {item.rating}</b> <br />
                            <b>Durasi: {item.duration}</b> <br />
                            <b>genre: {item.genre}</b>
                            <b>tahun: {item.year}</b> <br /> <br />
                            <b>deskripsi:</b> {item.description} <br /> <br />
                            <button onClick={handleEdit} value={item.id}>Edit</button>
                            &nbsp;
                            <button onClick={handleDelete} value={item.id}>Delete</button>
                        </div>
                    )
                })
            }

        {/* Form */}
        <h1 style={{textAlign: "center"}}>Form Movie List Editor</h1>

        <div style={{width: "50%", margin: "0 auto", display: "block"}}>
            <div style={{border: "1px solid #aaa", padding: "20px"}}>
                <form onSubmit={handleSubmit}>
                    <label style={{float: "left"}}>
                        Judul:
                    </label>
                    <input style={{float: "right"}} type="text" name="title" value={input.title} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        Rating
                    </label>
                    <input style={{float: "right"}} type="number" name="rating" min="1" max="10" value={input.rating} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        Durasi (dalam menit):
                    </label>
                    <input style={{float: "right"}} type="number" name="duration" value={input.duration} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        genre:
                    </label>
                    <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        tahun:
                    </label>
                    <input style={{float: "right"}} type="number" name="year" value={input.year} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        deskripsi:
                    </label>
                    <textarea style={{float: "right"}} type="text" name="description" value={input.description} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <br/>
                    <div style={{width: "100%", paddingBottom: "20px"}}>
                        <button style={{ float: "right"}}>submit</button>
                    </div>
                </form>
            </div>
        </div>
    </>
    )
}

export default MovieListEditor
